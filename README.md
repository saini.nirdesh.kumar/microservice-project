# OverView
- Till Now, We Build a complete CICD Pipeline using GitLab CICD which includes -
    - Run Unit Test
    - Run SAST Test
    - Build Docker Image
    - Push to Docker Registry
    - Deploy to Dev Server
    - Deploy to Staging Server
    - Deploy to Prod
    
- Ref - https://gitlab.com/saini.nirdesh.kumar/GitLab-CICD
# MicroServices 
- Microservices became the standard for modern cloud appplications.
- Monolith Architecture -
    - Before Microservices, A monolith architecture was the standard which means all components are the part of single unit (code).
    - Everything is developed, deployed and scaled as single unit means all code will be written in the single tech stack.
    - It will generate the 1 single artifact which redeploy the entire application on each update.
    - Application is too large and complex
    - You can only scale the entire app not specific service
    - Release Process take longer

- Microservices Architecture - 
    - Split application into smaller independent service based on business functionalities.
    - Each service developed, deployed and scaled seprately (Loosely Coupled).
    - Only Updated Microservice needs to be released not entire application. 
    - Microservice Communicate to each other by API Calls.

- For storing a complete application code, we use single repository but For storing microservices code, we have 2 options - MonoRepo and PolyRepo 

- MonoRepo (single Repository) -
    - Monorepo means use 1 git repo that contains many services/projects. 
    - Here MonoRepo contains, 1 directory for each microservices.
    - It makes the code management and development easier.
    - Changes Can be tracked, tested and released together.
    - No tight coupling of projects.
    - Easier to break this criteria and develop more tightly coupled code.
    - Big Source Code, means git interactios become slow.
    - Additional logc necessary to make sure only service is build and deployed which had changes.

- PolyRepo (Multi Repo) -
    - Polyrepo means use multi git repo that contains many services/projects. 
    - Here PolyRepo contains, 1 git repo for each microservices.
    - Code is completely isolated.
    - Anyone can clone and work on them seprately.
    - In GitLab Groups, You have projects to configure connected projects together (Groups)
    - CICD becomes easy as each repo contains its pipeline script.

![MonoRepo-PolyRepo](images/monorepo-vs-polyrepo.png)

# Microservices - Demo Overview 
- We will configure CICD Pipeline for Microservices Application.

- We Have 3 Microservices in the application like -
    - Frontend
    - Shopping-Cart
    - Products

- Now, We will configure the pipeline for microservices application for both type of code organization like MonoRepo and PolyRepo.

![Microservice-Application](images/microservice-app.png)

# MonoRepo Project Overview and Its CICD
- There are 3 folders for the application - Frontend, Products and Shopping-Cart
- Each Microservice has its own dependencies to build image for the microservice.
- SetUp Configuration for CICD
    - GitLab Runner - we already configured them just enabled them for other project by going `GitLab-CICD -> Setting -> CICD -> Runner -> Edit -> Untick the lock variable -> save`
    - Deployment Server - we already created an ec2 server for nodeapp project with runner, docker and docker-compose installation
- In CICD, we will focus on those part which are specific to monorepo and microservice application like build, push and deploy. we will not include like test or other steps which common to all.
## Steps For CICD
- We are taking the specific stages which will be different from monolithic application like build, deploy
    1. we will create the stages - build and deploy
    2. now, we will create the job for build image and push image to docker registery for each microservices.
    3. now, we will configure the pipeline for changes like if changes in frontend then frontend job run only not all jobs.
    4. now, 1st time it will run all the jobs but in 2nd time only the changed service job will run.
        - for testing we added the comments in the products/server.js file and push the changes.
        - here only `build_products` job is running.
        - ![specific-job](images/specific-job.png)
    5. Frontend `Docekrfile` contains other microservice `container name` like `products_app_1` whose will create by `docker-compose` file and help frontend in networking.
    6. Adding `Docker-Compose.yaml` generic file so that we can deploy each microservice with single one file. 
    7. Adding `.deploy` template and using this template deploy the microservices.

### Ref - `.gitlab-ci.yaml`
     
